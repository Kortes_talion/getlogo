/** @requires BEM */
/** @requires BEM.DOM */

(function(undefined) {
    BEM.DOM.decl('b-top', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                function getRandomInt(min, max) {
                    return Math.floor(Math.random() * (max - min + 1)) + min;
                }

                var count = getRandomInt(1, 7);
                var bg = 'url(../../images/temp/top'+count+ '.jpg)';

                bemThis.domElem.css('background-image', bg);
            }
        }
    });
})();
