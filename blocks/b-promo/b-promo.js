/** @requires BEM */
/** @requires BEM.DOM */

(function(undefined) {
    BEM.DOM.decl('b-promo', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                var max = bemThis.elem('item').length;

                bemThis.elem('count-body').slider({
                    range: "min",
                    min: 2,
                    max: max,
                    value: 3,
                    slide: function( event, ui ) {
                        var count = 240 * ui.value;
                        bemThis.elem('body').css('height', count);
                    }
                });

                bemThis.elem('item').fancybox({
                    padding:0,
                    beforeShow : function() {
                        var index = bemThis.elem('item').index(this.element);
                        this.inner.find('.b-fancy__item').removeClass('b-fancy__item_state_active');
                        this.inner.find('.b-fancy__card').removeClass('b-fancy__card_state_active');
                        this.inner.find('.b-fancy__item').eq(index).addClass('b-fancy__item_state_active');
                        this.inner.find('.b-fancy__card').eq(index).addClass('b-fancy__card_state_active');
                    }
                });

            }
        }
    });
})();
