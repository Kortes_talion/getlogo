/** @requires BEM */
/** @requires BEM.DOM */

(function(undefined) {
    BEM.DOM.decl('b-fancy', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.elem('list').customCarousel({
                    displayed: 7,
                    itemWidth: 130,
                    carouselHeight: 160,
                    nextItemSelector: $('.b-fancy__arrow-list_position_next'),
                    prevItemSelector: $('.b-fancy__arrow-list_position_prev'),
                    statusClass: 'b-fancy__item_state_active'
                });

                bemThis.elem('arrow').click(function(e){
                    e.preventDefault();
                    var max = $('.b-fancy__card').length - 1;
                    var index = $('.b-fancy__card_state_active').index('.b-fancy__card');
                    if ( $(this).hasClass('b-fancy__arrow_position_next')){
                        index += 1;
                    } else {
                        index -= 1;
                    };
                    if (index > max) {
                        index = 0;
                    };
                    if (index < 0) {
                        index = max;
                    };
                    $('.b-fancy__card').removeClass('b-fancy__card_state_active');
                    $('.b-fancy__item').removeClass('b-fancy__item_state_active');
                    $('.b-fancy__card:eq('+index+')').addClass('b-fancy__card_state_active');
                    $('.b-fancy__item:eq('+index+')').addClass('b-fancy__item_state_active');
                });

                bemThis.elem('promo-list-link').click(function(e){
                    e.preventDefault();
                    var index = $(this).closest('.b-fancy__item').index('.b-fancy__item');
                    $('.b-fancy__card').removeClass('b-fancy__card_state_active');
                    $('.b-fancy__item').removeClass('b-fancy__item_state_active');
                    $('.b-fancy__card:eq('+index+')').addClass('b-fancy__card_state_active');
                    $('.b-fancy__item:eq('+index+')').addClass('b-fancy__item_state_active');
                });_
            }
        }
    });
})();
