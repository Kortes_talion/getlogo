({
    mustDeps: [
        {
            block: 'b-grid'
        },
        {
            block: 'b-grid',
            elem: 'cell'
        },
        {
            block: 'b-grid',
            elem: 'cell',
            elemMods:{span: '6'}
        },
        {
            block: 'b-logo'
        }
    ]
})