({
    block: 'b-page',
    'x-ua-compatible': false,
    title: 'Главная страница',
    head: [
        { elem: 'js', url: '../../js/jquery-1.10.0.min.js'},
        { elem: 'css', url: 'index.css'}
    ],
    content: [
        {
            block: 'b-top'
        },
        {
            block: 'b-page-inner',
            content: [
                {
                    block: 'b-header'
                },
                {
                    block: 'b-content',
                    content: [
                        {
                            elem: 'info',
                            content: {
                                block: 'b-grid',
                                content: [
                                    {
                                        elem: 'cell',
                                        elemMods: {span: '4'},
                                        content: {
                                            block: 'b-info',
                                            image: '../../images/design/info1.png',
                                            hover: '../../images/design/hover-info1.png',
                                            content: 'Мы —  специалисты айдентики, которые хорошо знают свое дело и любят свою работу'
                                        }
                                    },
                                    {
                                        elem: 'cell',
                                        elemMods: {span: '4'},
                                        content: {
                                            block: 'b-info',
                                            image: '../../images/design/info2.png',
                                            hover: '../../images/design/hover-info2.png',
                                            content: 'Наши логотипы — осмысленные знаки, это: интересные метафоры, и грамотная типографика'
                                        }
                                    },
                                    {
                                        elem: 'cell',
                                        elemMods: {span: '4'},
                                        content: {
                                            block: 'b-info',
                                            image: '../../images/design/info3.png',
                                            hover: '../../images/design/hover-info3.png',
                                            content: 'Мы понимаем настроение, замечаем нюансы, и обходим потенциальные сложности'
                                        }
                                    }
                                ]
                            } 
                        },
                        {
                            elem: 'text',
                            content: 'Если вам нужно придумать название, создать логотип, оформить сайт, или составить фирменный стиль, то вы, определенно, оказались в нужном месте.  А здесь рассказано <a href="#">сколько это стоит и почему.</a>'
                        },
                        {
                            block: 'b-promo',
                            images: [
                                {
                                    image: '../../images/temp/1.jpg',
                                    title: 'Smartube'
                                },
                                {
                                    image: '../../images/temp/2.jpg',
                                    title: 'Ассистент'
                                },
                                {
                                    image: '../../images/temp/3.jpg',
                                    title: 'КлубНика'
                                },
                                {
                                    image: '../../images/temp/4.jpg',
                                    title: 'Агапроект'
                                },
                                {
                                    image: '../../images/temp/1.jpg',
                                    title: 'Smartube'
                                },
                                {
                                    image: '../../images/temp/2.jpg',
                                    title: 'Ассистент'
                                },
                                {
                                    image: '../../images/temp/3.jpg',
                                    title: 'КлубНика'
                                },
                                {
                                    image: '../../images/temp/4.jpg',
                                    title: 'Агапроект'
                                }
                            ]
                        }
                    ]
                },
                {
                    elem: 'footer'
                }
            ]
        },
        {
            block: 'b-footer'
        },
        {
            block: 'b-fancy',
            content: {
                elem: 'promo',
                list: [
                    {
                        title: 'Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo1.png',
                        link: 'www.ustaholels.ru',
                        text: '<h3>Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    },
                    {
                        title: '2Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo2.png',
                        link: 'www.2ustaholels.ru',
                        text: '<h3>2Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    },
                    {
                        title: '3Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo3.png',
                        link: 'www.3ustaholels.ru',
                        text: '<h3>3Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    },
                    {
                        title: '4Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo4.png',
                        link: 'www.4ustaholels.ru',
                        text: '<h3>4Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    },
                    {
                        title: '5Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo5.png',
                        link: 'www.5ustaholels.ru',
                        text: '<h3>5Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    },
                    {
                        title: '6Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo6.png',
                        link: 'www.6ustaholels.ru',
                        text: '<h3>6Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    },
                    {
                        title: '7Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo7.png',
                        link: 'www.7ustaholels.ru',
                        text: '<h3>7Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    },
                    {
                        title: '8Логотип сервиса «USTA Hotels»',
                        image: '../../images/temp/promo8.png',
                        link: 'www.8ustaholels.ru',
                        text: '<h3>8Логотип представляет собой открытый журнал учета постояльцев.</h3><p>Его цель — подчеркнуть принадлежность всего проекта вцелом к USTA group, а также, задать всему графическому языку сайта собственную «физику». Логотип представляет собой открытый журнал учета постояльцев.</p>'
                    }
                ]
            }
        },
        {
          block: 'b-scripts',
          content: [
            { elem: 'js', url:'_index.js'}
          ]
        }
    ]
})