Bem Bootstrap project для быстрого старта разработки нового проекта по методологии bem
========================

    Включает уровни переопределения для библиотеки bem-bl
    Структура проекта:

        * blocks - Для блоков проекта
        * pages - Папка со страницами проекта
        * libs включает в себя библиотеки bem-bl и lib-lb.

        clever-bem-lib - Это уровень переопределения библиотеки bem-bl. В ней размещаются общие блоки которые можно ремспользовать на всех проектах.

        Если хотите добавить туда чтото - шлите пул реквесты

* Создание проекта

        git clone git@bitbucket.org:cleversite/clever-bem-bootstrap.git project_name
        cd project_name
        rm -rf .git

        где project_name название нового проекта

* Установить зависимости.

        npm install

* Мейк

        bem make
        Первоночальное выполнение команды bem make само инициализирует нужные библиотеки bem-bl и clever-bem-lib

* ИНициализируем репозиторий проекта

        git init


[bem.info](http://bem.info).
[bem клуб](http://clubs.ya.ru/bem/).
[bem tools](https://github.com/bem/bem-tools/blob/master/README.ru.md).
[bem-bl](http://bem.github.com/bem-bl/index.ru.html)
[clever-bem-lib](https://bitbucket.org/cleversite/clever-bem-lib).
